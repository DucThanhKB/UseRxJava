package com.ducthanh.demodowloadfileuserxjava.loadImageActivity;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by DucThanh on 7/14/2017.
 */

public class LoadImageModel {
    private String mPath = "/sdcard/downloadedfile.jpg";
    private LoadImageInterface.PresenterInterface mPresenter;

    public LoadImageModel(Context context, LoadImageInterface.PresenterInterface mPresenter) {
        this.mPresenter = mPresenter;
    }
    //download Image with url and store in sdcard
    public String getImage() throws IOException {
        int count;
        URL url = new URL("https://i.imgur.com/tGbaZCY.jpg");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();
        InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);
        OutputStream outputStream = new FileOutputStream(mPath);
        byte data[] = new byte[1024];
        while ((count = inputStream.read(data)) != -1) {
            outputStream.write(data, 0, count);
        }
        // flushing output
        outputStream.flush();
        // closing streams
        outputStream.close();
        inputStream.close();
        return "complete";
    }
}
