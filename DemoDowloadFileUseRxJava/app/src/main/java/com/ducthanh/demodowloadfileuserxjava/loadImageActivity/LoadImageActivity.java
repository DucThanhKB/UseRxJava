package com.ducthanh.demodowloadfileuserxjava.loadImageActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ducthanh.demodowloadfileuserxjava.R;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class LoadImageActivity extends AppCompatActivity implements LoadImageInterface.ViewInterface {

    @BindView(R.id.button_load_image)
    Button buttonLoadImage;
    private LoadImagePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter = new LoadImagePresenter(this, this);
        //Create Observable
        final Observable operationObservable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    subscriber.onNext(mPresenter.SendEventClickDowloadToModel());
                } catch (IOException e) {
                    Toast.makeText(LoadImageActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                }
                subscriber.onCompleted();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        //hanle click event
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationObservable.subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(String value) {
                        Toast.makeText(LoadImageActivity.this, "Completed", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
