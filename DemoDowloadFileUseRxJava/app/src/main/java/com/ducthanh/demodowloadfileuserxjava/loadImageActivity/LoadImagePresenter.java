package com.ducthanh.demodowloadfileuserxjava.loadImageActivity;

import android.content.Context;

import java.io.IOException;

/**
 * Created by DucThanh on 7/14/2017.
 */

public class LoadImagePresenter implements LoadImageInterface.PresenterInterface {

    private LoadImageInterface.ViewInterface mView;
    private LoadImageModel mModel;

    public LoadImagePresenter(Context context, LoadImageInterface.ViewInterface mView) {
        this.mView = mView;
        mModel = new LoadImageModel(context, this);
    }

    public String SendEventClickDowloadToModel () throws IOException {
        return mModel.getImage();
    }
}
